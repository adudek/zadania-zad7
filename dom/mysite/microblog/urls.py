from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.wszystkie_posty, name="wszystkie_posty"),
    url(r'^uzytkownicy/(?P<id>\d+)$', views.posty_uzytkownika, name="posty_uzytkownika"),
    url(r'^zaloguj$', views.logowanie, name="logowanie"),
    url(r'^wyloguj$', views.wylogowanie, name="wylogowanie"),
    url(r'^wpisy/nowy', views.nowy_wpis, name="nowy_wpis")
)