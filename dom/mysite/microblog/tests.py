#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import connection
from django.contrib.auth.models import User
from microblog.models import Post
from django.utils import unittest
from django.utils import timezone
from django.test import Client

import re

login = "TEST"
haslo = "TEST@"

client = Client()

class SampleTestCase(unittest.TestCase):
    # do PUNKTU 1
    def test_database_existence(self):
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM microblog_posts;')
        self.assertNotEqual(cursor.fetchall(), None)

class SampleTestCase2(unittest.TestCase):
    def test_user_messages(self):

        user = User.objects.create(username=login)
        user.set_password(haslo)
        user.save()

        post = Post(
            title='tytul',
            text='tekst',
            pub_date = timezone.now(),
            user_id = 1
        )
        post.save()

        response = client.get("/blog/")
        self.assertNotEqual(response.status_code, 404)
        self.assertNotEqual(response.content, None)
