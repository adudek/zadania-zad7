from django.contrib.auth.models import User
from django.db import models

class Entry(models.Model):
    text = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField()
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.title