#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.shortcuts import render, get_list_or_404, redirect
from django.contrib import messages
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from microblog.models import Entry

def wszystkie_posty(request, message=None):
    wpisy = Entry.objects.order_by('-pub_date')
    return render(request, 'microblog/wszystkie_posty.html', {'wpisy': wpisy, 'message': message})

def posty_uzytkownika(request, id):
    uzytkownik = User.objects.all().filter(pk=id)
    if uzytkownik:
        wpisy = Entry.objects.all().filter(user_id=id).order_by('-pub_date')
        return render(request, 'microblog/posty_uzytkownika.html', {'wpisy': wpisy})
    raise Http404

def nowy_wpis(request):
    if not request.user.is_authenticated():
        return wszystkie_posty(request,'Musisz się zalogować!')
    if request.method == "POST":
        if request.POST.get('tytul') and request.POST.get('tekst'):
            post = Entry(title=request.POST.get('tytul'), text=request.POST.get('tekst'), pub_date = timezone.now(), user_id = request.user.id)
            post.save()
            return wszystkie_posty(request,'Dodano wpis!')
        else:
            return render(request, 'microblog/nowy_post.html', {
                'message': 'Wypełnij wszystkie pola'
            })
    else:
        return render(request, 'microblog/nowy_post.html', {})

def logowanie(request):
    if request.user.is_authenticated():
        return wszystkie_posty(request,'Jesteś już zalogowany!')
    if request.method == "POST":
        uzytkownik = authenticate(username=request.POST.get('login'), password=request.POST.get('haslo'))
        if uzytkownik:
            login(request, uzytkownik)
            return wszystkie_posty(request,'Zalogowano!')
        else:
            return render(request, 'microblog/logowanie.html', {
                'message': 'Błędny login lub hasło!'
            })
    else:
        return render(request, 'microblog/logowanie.html', {
            'title': 'Zaloguj się'
        })

def wylogowanie(request):
    if request.user.is_authenticated():
        logout(request)
        return wszystkie_posty(request,'Wylogowano!')
    else:
        return wszystkie_posty(request,'Nie jesteś zalogowany!')
